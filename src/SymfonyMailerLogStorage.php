<?php

namespace Drupal\symfony_mailer_log;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Storage handler for symfony_mailer_log entities.
 */
class SymfonyMailerLogStorage extends SqlContentEntityStorage implements SymfonyMailerLogStorageInterface {}
