<?php

namespace Drupal\symfony_mailer_log;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Interface for SymfonyMailerLogStorage class.
 */
interface SymfonyMailerLogStorageInterface extends ContentEntityStorageInterface {}
